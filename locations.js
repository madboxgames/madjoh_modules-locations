define([
	'require',
	'madjoh_modules/ajax/ajax',
	'madjoh_modules/wording/wording',
],
function(require, AJAX, Wording){
	var Locations = {
		getLocations : function(callback){
			return AJAX.post('/locations');
		},
		fillLocationSelect : function(result){
			// TODO : Move This to Account Update Page
			var selects = document.querySelectorAll('select.location-list');
			for(var i = 0; i < selects.length; i++){
				selects[i].innerHTML = '';

				var option = document.createElement('option');
					option.innerHTML = Wording.getText('country');
					option.value = 0;
					option.selected = true;
					option.disabled = true;

				selects[i].appendChild(option);

				for(var j = 0; j < result.data.locations.length; j++){
					var option = document.createElement('option');
					option.innerHTML = result.data.locations[j].name;
					option.value = result.data.locations[j].id;

					selects[i].appendChild(option);
				}
			}
		}
	};
	
	return Locations;
});